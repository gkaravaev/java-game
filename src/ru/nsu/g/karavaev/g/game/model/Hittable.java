package ru.nsu.g.karavaev.g.game.model;

public interface Hittable {

    public void getHit(Projectile projectile);
}
